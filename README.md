## Jumping in D ##
### Purpose ###
#### Target audience ####
If you are getting close to D there are a number of resources you can count on: the official documentation on [dlang.org](http://dlang.org/), [books](https://duckduckgo.com/?q=!ebay+d+programming+language+alexandrescu), the friendly folks on the [IRC Freenode channel](http://webchat.freenode.net?randomnick=1&channels=%23d). However, as you open your text editor and start typing code, you will face common issues that will raise questions and doubts, in spite of what you have already learned. This small projects aims to show you how a simple SDL project can tackle some of those issues, hopefully getting you started into designing you code in D.

#### What's in this code? ####
This sample program is not an all-you-need-to-learn-D kind of reference. I'm assuming you already know a bit about D, but you're still fresh enough to be confused at some basic topics. Among the other things, this program demonstrates:
* creating a mixed C and D project with CMake
* writing an SDL program in D
* calling C functions and managing the acquired resources
* managing your custom resources
* inheriting from a base class providing both an abstract interface and a default implementation for some methods
* loading textures in SDL using the new [BPG](http://libbpg.org/) image format
* struct vs class

More examples could be added in the future, although I don't want to clutter this project with too much code.

#### Dude, is this an hello world app or what? ####
If you found this example trivial, then maybe you're coding skills in D are above that of my target audience :) But if you have suggestions or you spot any mistake, please do get in touch!

### Compiling ###
#### Required tools ####
I've only tested this code with gdc 5.2.0 on Linux. It might work with other compilers but please consider it untested.
On my system I'm using:
* gdc 5.2.0
* cmake 3.0 (patched to support D - see https://github.com/trentforkert/cmake)

#### Compiling instructions ####
1. In a new directory, type <code>cmake -DCMAKE_BUILD_TYPE=Debug &lt;path_to_jumping-in-d_code&gt;</code>
2. Run <code>make</code>
3. To run the program, type <code>./hello_d</code>
