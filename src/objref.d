/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module objref;

template TObjRef(T) {
	static if (T.sizeof > size_t.sizeof) {
		struct ObjRef {
			this(ref T parMainObj) {
				m_ref = &parMainObj;
			}

			ref inout(T) get_ref() inout { return *m_ref; }
			alias get_ref this;

			private T* m_ref;
		}
	}
	else {
		alias ObjRef = T;
	}
}
