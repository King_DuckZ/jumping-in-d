/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module gameobjjumping;
public import gameobj;
import derelict.sdl2.sdl;
import texture;
import std.stdio;
import collisionline;
import collidingentity : CollidingEntity;

//This is one possible game scene. It will draw a texture on the screen
//and it will report user wants to quit if you press the close button in the
//window decoration.
class GameObjJumping : GameObj {
	//Don't disable this(), it's disabled automatically when you define a
	//custorm constructor.
	//@disable this();

	//Take the pointer to the renderer and set initial values
	this (SDL_Renderer* parRenderer) {
		super(parRenderer);
		m_wants_to_quit = false;
	}

	//Remember: class destructors are not guaranteed to be called
	//deterministically, so all resource-freeing code should rather go in the
	//dispose() method.
	~this() {
		writeln("Destroying GameObj");
		//m_texture's type is a struct, so destruction is implicit at this point
	}

	override void prepare() {
		string texture_name = "bcruiser_normal.bpg";
		writeln("Loading texture %s", texture_name);

		//Note how this is doing a swap and not a copy. You can verify that
		//in the output. The first "Destroying texture  (null)" message is the
		//temporary created here, which took m_texture's content (null) and gets
		//destroyed as it goes out of scope.
		//The following code for example won't compile as it would trigger a
		//copy construction, but this(this) is marked as @disable in Texture:
		//  auto tmp = Texture(texture_name, renderer());
		//  m_texture = tmp;
		m_texture = Texture(texture_name, renderer());
	}

	override void dispose() {
		writeln("GameObjJumping dispose()");
		//m_texture.dispose() is what you would normally expect to find here,
		//but to demonstrate the use of clear() from the caller code, this
		//method is intentionally empty.
		//You can verify the program's output as you quit by uncommenting the
		//following line:
		//m_texture.dispose();
	}

	//This is the function that is called at every frame. It represents the
	//main loop's body.
	override void exec() {
		//Move the sprite around (note that we store the original position)
		vec2 offs = vec2(0.0f);
		m_coll_sprite += offs;

		//Check for collision between our sprite and the screen edges
		//vec2 collision_point;
		//if (m_coll_sprite.collides(m_coll_screen, collision_point)) {
			//assert(false); //not implemented
		//}

		//Draw the sprite
		SDL_RenderClear(renderer());
		m_texture.draw();
		SDL_RenderPresent(renderer());
		m_wants_to_quit = do_events();
	}

	override bool wants_to_quit() const {
		return m_wants_to_quit;
	}

private:
	Texture m_texture;
	CollisionQuad m_coll_screen;
	CollidingEntity m_coll_sprite;
	bool m_wants_to_quit;
}

private bool do_events() {
	SDL_Event eve;
	while (SDL_PollEvent(&eve)) {
		switch (eve.type) {
		case SDL_KEYDOWN:
			break;

		case SDL_KEYUP:
			break;

		case SDL_QUIT:
			return true;

		default:
			break;
		}
	}
	return false;
}
