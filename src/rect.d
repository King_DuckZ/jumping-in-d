/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module rect;
import std.traits : isNumeric, isAssignable;

struct Rect(T) {
	this (in T parLeft, in T parTop, in T parRight, in T parBottom) {
		m_left = parLeft;
		m_top = parTop;
		m_right = parRight;
		m_bottom = parBottom;
	}

	@property ref inout(T) left() inout { return m_left; }
	@property ref inout(T) top() inout { return m_top; }
	@property ref inout(T) right() inout { return m_right; }
	@property ref inout(T) bottom() inout { return m_bottom; }
	static if (isAssignable!T) {
		@property ref T left(in T parLeft) { return m_left = parLeft; }
		@property ref T top(in T parTop) { return m_top = parTop; }
		@property ref T right(in T parRight) { return m_right = parRight; }
		@property ref T bottom(in T parBottom) { return m_bottom = parBottom; }
	}
	static if (isNumeric!T) {
		@property diff_t width() const { return m_right - m_left; }
		@property diff_t height() const { return m_bottom - m_top; }
	}

private:
	static if (isNumeric!T) {
		alias diff_t = typeof(m_right - m_left);
	}

	T m_left;
	T m_top;
	T m_right;
	T m_bottom;
}
