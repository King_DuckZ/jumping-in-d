/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module collisionline;
public import vector : Vector;
import rect : Rect;
import objref;

alias vec2 = Vector!(float, 2);

struct CollisionLine {
	this (vec2 parA, vec2 parB) {
		m_from = parA;
		m_to = parB;
		assert(m_from != m_to);
	}

	@property ref const(vec2) from() const { return m_from; }
	@property ref const(vec2) to() const { return m_to; }

	//This is how you provide your own operator+= and operator-= in D
	ref CollisionLine opOpAssign(string op)(ref const(vec2) parOffset) {
		static if ("+" == op || "-" == op) {
			mixin("m_from "~op~"= parOffset;");
			mixin("m_to "~op~"= parOffset;");
			return this;
		}
		else {
			static assert(false, "No CollisionLine::operator"~op~" available");
		}
	}

private:
	vec2 m_from;
	vec2 m_to;
}

struct CollisionQuad {
	Rect!CollisionLine m_lines;

	//Structs don't have inheritance, but you can achieve the same with:
	alias m_lines this;
}

alias CollisionQuadRef = TObjRef!(const(CollisionQuad)).ObjRef;
alias CollisionLineRef = TObjRef!(const(CollisionLine)).ObjRef;
alias vec2ref = TObjRef!(const(vec2)).ObjRef;

bool collides (CollisionLineRef parLine, CollisionLineRef parLine1, vec2ref parOffs, out vec2 parPoint) {
	parPoint = vec2(0.0f);
	return false;
}

bool collides (CollisionQuadRef parQuad, CollisionQuadRef parQuad1, vec2ref parOffs, out vec2 parPoint) {
	return false;
}
