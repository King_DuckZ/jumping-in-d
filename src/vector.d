/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module vector;
import std.algorithm : fill;

struct Vector(T, int N) {
	//With the if(condition) before the function body we can tell the compiler
	//this particular template should only be considered when condition is true.
	this(P...)(P parInit) if (N == parInit.length) {
		int index = 0;
		foreach(p;parInit) {
			m_data[index++] = p;
		}
	}

	this(P...)(P parInit) if (N > 1 && 1 == parInit.length) {
		//m_data is a static array, by slicing it we get a dynamic array and
		//fill that one. You will get a build error if you dont add the [].
		fill(m_data[], parInit[0]);
	}

	//Define mathematical operators
	//Note the use of auto ref. This is only allowed in function templates, and
	//instructs the compiler to generate a byref and byvalue version of the same
	//function. It is necessary to have a byvalue function; if you don't provide
	//one, the following code: v += Vector!(int, 2)(1, 2);
	//won't compile as the temporary created on the rhs can't be bound to a
	//const ref in D.
	ref Vector!(T,N) opOpAssign(string op)(auto ref const(Vector!(T,N)) parOther) {
		for (int z = 0; z < N; ++z) {
			mixin("m_data[z] "~op~"= parOther.m_data[z];");
		}
		return this;
	}
	ref Vector!(T,N) opOpAssign(string op)(T parOther) {
		for (int z = 0; z < N; ++z) {
			mixin("m_data[z] "~op~"= parOther;");
		}
		return this;
	}
	Vector!(T,N) opBinary(string op)(Vector!(T,N) parOther) {
		for (int z = 0; z < N; ++z) {
			mixin("parOther.m_data[z] = m_data[z] "~op~" parOther.m_data[z];");
		}
		return parOther;
	}
	Vector!(T,N) opBinary(string op)(T parOther) {
		auto retval = Vector!(T,N)(this);
		for (int z = 0; z < N; ++z) {
			mixin("retval "~op~"= parOther;");
		}
		return parOther;
	}

	//Define x, y, z, w accessors. Vectors larger than 4 elements won't have any
	//of those.
	static if (N <= 4) {
		//Getter
		@property T x() const { return m_data[0]; }
		//Setter
		@property T x(in T parVal) { return m_data[0] = parVal; }
		static if (N >= 2) {
			@property T y() const { return m_data[1]; }
			@property T y(in T parVal) { return m_data[1] = parVal; }
		}
		static if (N >= 3) {
			@property T z() const { return m_data[2]; }
			@property T z(in T parVal) { return m_data[2] = parVal; }
		}
		static if (N >= 4) {
			@property T w() const { return m_data[3]; }
			@property T w(in T parVal) { return m_data[3] = parVal; }
		}
	}

	//Other accessors that can make using vectors more convenient - same static
	//if as above, but written here to separate code for the sake of clarity.
	static if (N <= 4) {
		@property Vector!(T, 2) x0() const { return Vector!(T, 2)(x, T(0)); }
		@property Vector!(T, 2) x1() const { return Vector!(T, 2)(x, T(1)); }
		@property Vector!(T, 2) _0x() const { return Vector!(T, 2)(T(0), x); }
		@property Vector!(T, 2) _1x() const { return Vector!(T, 2)(T(1), x); }

		static if (N > 1) {
			@property Vector!(T, 2) xy() const { return Vector!(T, 2)(x, y); }
			@property Vector!(T, 2) y0() const { return Vector!(T, 2)(y, T(0)); }
			@property Vector!(T, 2) y1() const { return Vector!(T, 2)(y, T(1)); }
			@property Vector!(T, 2) _0y() const { return Vector!(T, 2)(T(0), y); }
			@property Vector!(T, 2) _1y() const { return Vector!(T, 2)(T(1), y); }
		}
		static if (N > 2) {
			@property Vector!(T, 3) xyz() const { return Vector!(T, 3)(x, y, z); }
			@property Vector!(T, 3) zxy() const { return Vector!(T, 3)(z, x, y); }
			@property Vector!(T, 3) yzx() const { return Vector!(T, 3)(y, z, x); }
			@property Vector!(T, 3) xzy() const { return Vector!(T, 3)(x, z, y); }
			@property Vector!(T, 3) zyx() const { return Vector!(T, 3)(z, y, x); }
			@property Vector!(T, 3) yxz() const { return Vector!(T, 3)(y, x, z); }
			//add more if you need
		}
	}

	//Overload operator[] to allow accessing the vector with array syntax
	//inout tells the compiler to generate both a const and a non-const
	//version of this method.
	ref inout(T) opIndex (size_t parIndex) inout {
		assert(parIndex < N);
		return m_data[parIndex];
	}

private:
	T m_data[N];
}

//You can write unit tests inside unittest {} blocks. These will only be
//compiled if you specify the -funittest switch (gdc). Unit tests are run as
//your program starts, but needs to be grammatically correct even if you don't
//use the -funittest switch.
unittest {
	auto v1 = Vector!(int, 3)(2);
	assert(2 == v1.x);
	assert(2 == v1.y);
	assert(2 == v1.z);

	auto v2 = Vector!(int, 4)(3, 4, 5, 6);
	assert(3 == v2.x);
	assert(4 == v2.y);
	assert(5 == v2.z);
	assert(6 == v2.w);
	assert(Vector!(int, 2)(3, 0) == v2.x0);
	assert(Vector!(int, 2)(4, 0) == v2.y0);
	assert(Vector!(int, 3)(5, 4, 3) == v2.zyx);

	v2 += Vector!(int, 4)(1, 2, 3, 4);
	assert(4 == v2.x);
	assert(6 == v2.y);
	assert(8 == v2.z);
	assert(10 == v2.w);
}
