/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module main;
import std.stdio;
import std.string;
import std.conv;
import derelict.sdl2.sdl;
import gameobjjumping;
import std.typecons : scoped; //Only import scoped from std.typecons module

void run_main_loop (GameObj parGame) {
	parGame.prepare();
	do {
		parGame.exec();
	} while (!parGame.wants_to_quit());
	parGame.dispose();
}

int main() {
	writeln("Hello world!");

	//Initialize DerelictSDL2 and SDL2 itself
	DerelictSDL2.load();
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		throw new Exception(
			format("Error initializing SDL: %s", text(SDL_GetError()))
		);
	}
	//Automatically uninitialize SDL2 on quitting - note that scope(exit)
	//statements are called in reverse order, so SDL_Quit() will be the last.
	scope(exit) { writeln("Destroying SDL..."); SDL_Quit(); }

	//Create a window...
	SDL_Window* win = SDL_CreateWindow("hello_d", 100, 100, 120, 120, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	scope(exit) { writeln("Destroying window..."); SDL_DestroyWindow(win); }

	//...and a renderer
	SDL_Renderer* renderer = SDL_CreateRenderer(win, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	scope(exit) { writeln("Destroying renderer..."); SDL_DestroyRenderer(renderer); }

	//This is our game object
	//Because GameObjJumping implementation purposedly doesn't destroy resources
	//in its dispose method (for this example's sake), you must make sure its
	//destructor gets called. You have several choices:
	//
	//1 - call clear() (deprecated and removed in recent D compilers)
	//  GameObj game = new GameObjJumping(renderer);
	//  scope(exit) clear(game);
	//
	//2 - call destroy()
	//note that while game.destroy() is semantically equivalent to the
	//destroy(game) syntax, you should never write it that way in case game
	//provides its own destroy() method. In that case the method will be
	//preferred, and you will end up doing something potentially different than
	//what you intended to. For the same reason, don't provide a destroy()
	//method in your classes.
	//  GameObj game = new GameObjJumping(renderer);
	//  scope(exit) destroy(game);
	//
	//3 - use scope (deprecated)
	//  scope GameObj game = new GameObjJumping(renderer);
	//
	//4 - use scoped (no need for new)
	//  auto game = scoped!GameObjJumping(renderer);

	auto game = scoped!GameObjJumping(renderer);
	run_main_loop(game);
	return 0;
}
