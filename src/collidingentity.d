/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module collidingentity;
import collisionline;

struct CollidingEntity {
	@property ref const(CollisionQuad) current() const { return m_current; }

	ref CollidingEntity opOpAssign(string op)(ref const(vec2) parOffset) {
		static if ("+" == op || "-" == op) {
			m_offs = parOffset;
			mixin("m_current.left "~op~"= parOffset;");
			mixin("m_current.top "~op~"= parOffset;");
			mixin("m_current.right "~op~"= parOffset;");
			mixin("m_current.bottom "~op~"= parOffset;");
			return this;
		}
		else {
			static assert(false, "No CollidingEntity::operator"~op~" available");
		}
	}

private:
	CollisionQuad m_current;
	vec2 m_offs;
}
