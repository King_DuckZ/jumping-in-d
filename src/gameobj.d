/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module gameobj;
import derelict.sdl2.sdl;

//We want to provide an interface for all game scenes. A game scene should
//implement some basic methods, such as prepare() and exec(), so the main loop
//can call them.
//We also want our interface to store a renderer. This forces us to declare
//an abstract class instead of an interface, becaues interfaces can't have
//constructors nor properties.
abstract class GameObj {
public:
	this (SDL_Renderer* parRenderer) {
		m_renderer = parRenderer;
		assert(m_renderer);
	}

	//Derived classes must implement those;
	//failure to do so results in a compilation error.
	//You need to mark them as "abstract" to let the compiler know there is
	//no implementation for them. If you don't mark them as abstract and you
	//forget to implement them in the derived game object, you will get a
	//linker error instead, and potentially the wrong behaviour as you call them
	//from a pointer to the base class.
	abstract void prepare();
	abstract void dispose();
	abstract void exec();
	abstract bool wants_to_quit() const;

protected:
	//Allow game objects inheriting from GameObj to retrieve the renderer they
	//stored through GameObj's constructor. final means it can't be overridden
	//(so no v-table entry required for this method); @property means you must
	//not use () when you call renderer on an object, eg: r=my_game_obj.renderer
	@property final SDL_Renderer* renderer() {
		return m_renderer;
	}

//Remember: private members can still be accessed from within this module.
//Or, from a different point of view if you come from a c++ background, within
//the gameobj module, everything is friend to everything else.
//One more good reason for putting each class into a different module.
private:
	SDL_Renderer* m_renderer;
}
