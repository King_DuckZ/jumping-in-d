/* Copyright 2015, Michele Santullo
 * This file is part of "Jumping in D".
 *
 * "Jumping in D" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Jumping in D" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Jumping in D".  If not, see <http://www.gnu.org/licenses/>.
 */

module texture;
import bpg;
import derelict.sdl2.sdl;
import std.file;
import std.stdio;

struct Texture {
public:
	@disable this(this);
	//@disable void opAssign(Texture);

	this (in string parPath, SDL_Renderer* parRenderer) {
		auto data = read(parPath);
		auto img = decode_bpg_image(cast(char[])data);

		SDL_Surface* surf = null;
		surf = SDL_CreateRGBSurfaceFrom(img.data.ptr, img.width, img.height, 24, 3 * img.width, 0xff, 0xff00, 0xff0000, 0);
		scope(exit) SDL_FreeSurface(surf);

		m_texture = SDL_CreateTextureFromSurface(parRenderer, surf);
		m_filename = parPath;
		m_renderer = parRenderer;
		m_imgrect.x = m_imgrect.y = 0;
		m_imgrect.w = img.width;
		m_imgrect.h = img.height;
	}

	~this() {
		writefln("Destroying texture %s (%s)", m_filename, m_texture);
		dispose();
	}

	void dispose() {
		if (m_texture) {
			SDL_DestroyTexture(m_texture);
			m_texture = null;
		}
	}

	void draw() {
		SDL_RenderCopy(m_renderer, m_texture, &m_imgrect, &m_imgrect);
	}

	bool empty() const {
		return null == m_texture;
	}

private:
	SDL_Renderer* m_renderer;
	SDL_Texture* m_texture;
	SDL_Rect m_imgrect;
	string m_filename;
}
